package xdp

import (
	//"errors"
	"errors"
	"fmt"
	"net"
	"os"

	"github.com/cilium/ebpf"
	"github.com/cilium/ebpf/link"
)

//go:generate go run github.com/cilium/ebpf/cmd/bpf2go xdp ../../ebpf/combined.c

type EgressInfo struct {
	Vid uint16
	Port uint16
}

type IngressMap map[uint32]uint32
type EgressMap map[uint32]EgressInfo

type ForwardMap struct {
	Ingress IngressMap
	Egress EgressMap
}

// Location where the BPF maps and programs are pinned so that are not delete when all BPF programs are detached from network devices
const (
	base_path = "/sys/fs/bpf/"
)

var (
	egress_map_path = base_path + "egress_map"
	ingress_map_path = base_path + "ingress_map"
	vm_map_path = base_path + "vm_map"

	egress_prog_path = base_path + "egress_prog"
	ingress_prog_path = base_path + "ingress_prog"
	vm_prog_path = base_path + "vm_prog"

	link_base = base_path + "progs/"
)

// Load() installs the BPF maps and programs
func Load() error {
	objs := xdpObjects{}
	if err := loadXdpObjects(&objs, nil); err != nil {
		return fmt.Errorf("unable to load XDP objects: %w", err)
	}

	// Persist all of the port maps
	if err := objs.xdpMaps.EgressPorts.Pin(egress_map_path); err != nil {
		return fmt.Errorf("unable to pin egress map: %w", err)
	}
	if err := objs.xdpMaps.IngressPorts.Pin(ingress_map_path); err != nil {
		return fmt.Errorf("unable to pin egress map: %w", err)
	}
	if err := objs.xdpMaps.VmPorts.Pin(vm_map_path); err != nil {
		return fmt.Errorf("unable to pin egress map: %w", err)
	}
	
	// Persist all BPF programs
	if err := objs.XdpEgressProg.Pin(egress_prog_path); err != nil {
		return fmt.Errorf("unable to pin egress xdp program: %w", err)
	}
	if err := objs.XdpIngressProg.Pin(ingress_prog_path); err != nil {
		return fmt.Errorf("unable to pin ingress xdp program: %w", err)
	}
	if err := objs.XdpVmProg.Pin(vm_prog_path); err != nil {
		return fmt.Errorf("unable to pin vm xdp program: %w", err)
	}

	return nil
}

// Unoad() removes all the BPF programs and maps
func Unload() error {
	var ret error = nil

	for _, f := range []string{egress_map_path, ingress_map_path, vm_map_path, egress_prog_path, ingress_prog_path, vm_prog_path} {
		err := os.Remove(f)
		if err != nil && !os.IsNotExist(err) {
			ret = err
		}
	}

	// remove all programs from net devices
	if err := os.RemoveAll(link_base); err != nil {
		ret = fmt.Errorf("%s: %w", link_base, err)
	}
	return ret
}

// equivlant of htons()
func bswap16(n uint16) uint16 {
	return ((n & 0xff) << 8) | ((n >> 8) & 0xff)
}

func getIngressMap() (IngressMap, error) {
	m, err := ebpf.LoadPinnedMap(ingress_map_path, nil)
	if err != nil {
		return nil, fmt.Errorf("unable to load map: %w", err)
	}
	defer m.Close()

	ret := make(IngressMap)
	entries := m.Iterate()
	var key, value uint32
	
	for entries.Next(&key, &value) {
		ret[key] = value
	}

	err = entries.Err()
	if err != nil {
		return nil, fmt.Errorf("error iterating over map: %w", err)
	}
	return ret, nil
}

func getEgressMap() (EgressMap, error) {
	m, err := ebpf.LoadPinnedMap(egress_map_path, nil)
	if err != nil {
		return nil, fmt.Errorf("unable to load map: %w", err)
	}
	defer m.Close()

	ret := make(EgressMap)

	entries := m.Iterate()
	var key uint32
	var value xdpOutputPort
	for entries.Next(&key, &value) {
		ret[key] = EgressInfo{Vid: bswap16(value.Vid), Port: value.Port}
	}

	err = entries.Err()
	if err != nil {
		return nil, fmt.Errorf("error iterating over map: %w", err)
	}
	return ret, nil
}

// GetForwardMap() returns the forwarding tables used by the XDP forwarding programs
func GetForwardMap() (*ForwardMap, error) {
	var err error
	ret := &ForwardMap{}
	ret.Egress, err = getEgressMap()
	if err != nil {
		return nil, err
	}
	ret.Ingress, err = getIngressMap()
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func installProg (pinpath string, ifindex int) error {
	prog, err := ebpf.LoadPinnedProgram(pinpath, nil)
	if err != nil {
		return fmt.Errorf("unable to load pinned program: %w", err)
	}
	// load BPF program into device
	var l link.Link
	l, err = link.AttachXDP(link.XDPOptions{
		Program: prog,
		Interface: int(ifindex),
	})
	if err != nil {
		return fmt.Errorf("unable to attach xdp program to device: %w", err)
	}
	err = os.MkdirAll(link_base, 0)
	if err != nil {
		return fmt.Errorf("unable to create directory: %s: %w", link_base, err)
	}
	fname := fmt.Sprintf("%s/progs/%d", base_path, ifindex)
	err = l.Pin(fname)
	if err != nil {
		return fmt.Errorf("unable to pin xdp program to link: %w", err)
	}
	return nil
}

func insertVM2VM(a, b int) error {
	m, err := ebpf.LoadPinnedMap(vm_map_path, nil)
	if err != nil {
		return fmt.Errorf("unable to load map: %s: %w", vm_map_path, err)
	}
	defer m.Close()

	key := uint32(a)
	val := uint32(b)
	err = m.Put(&key, &val)
	if err != nil {
		return fmt.Errorf("unable to put key: %w", err)
	}

	key = uint32(b)
	val = uint32(a)
	err = m.Put(&key, &val)
	if err != nil {
		return fmt.Errorf("unable to put key: %w", err)
	}

	err = installProg(vm_prog_path, a)
	if err != nil {
		return fmt.Errorf("unable to install BPF prog: %w", err)
	}
	
	err = installProg(vm_prog_path, b)
	if err != nil {
		return fmt.Errorf("unable to install BPF prog: %w", err)
	}

	return nil
}

// Insert() installs forwarding rules for interfaces specified by ifindex.
//
// ingress in the VM tap device, and egress is the hypervisor network interface. vid
// is the VLAN used to encapsulate traffic coming from the VM.
//
// If the VLAN is 0, this will directly connect two VM tap devices, with no encapsulation
func Insert(ingress, egress, vid int) error {
	var err error
	var emap *ebpf.Map
	var imap *ebpf.Map

	// special case, directly connect two VMs running on the same hypervisor
	if vid == 0 {
		return insertVM2VM(ingress, egress)
	}
	if vid > 4095 {
		return fmt.Errorf("VLAN out of range")
	}
	emap, err = ebpf.LoadPinnedMap(egress_map_path, nil)
	if err != nil {
		return fmt.Errorf("unable to load map: %w", err)
	}
	defer emap.Close()
	imap, err = ebpf.LoadPinnedMap(ingress_map_path, nil)
	if err != nil {
		return fmt.Errorf("unable to load map: %w", err)
	}
	defer imap.Close()

	// update forwarding tables
	key := uint32(vid)
	val := uint32(ingress)
	err = imap.Put(&key, &val)
	if err != nil {
		return fmt.Errorf("update ingress map: %w", err)
	}
	vlan := bswap16(uint16(vid)) // store in network byte order so the xdp program doens't need to convert for each packet
	key = uint32(ingress)
	err = emap.Put(&key, &xdpOutputPort{Vid: vlan, Port: uint16(egress)})
	if err != nil {
		return fmt.Errorf("update egress map: %w", err)
	}

	// install BPF program onto VM's tap device
	err = installProg(egress_prog_path, ingress)
	if err != nil {
		return err
	}

	// load BPF program onto egress net device if not already present
	// this is the hypervisor network interface that forwards packets back to the VMs
	fname := fmt.Sprintf("%s/%d", link_base, egress)
	_, err = os.Stat(fname)
	if os.IsNotExist(err) {
		err = installProg(ingress_prog_path, egress)
	}
	return err
}

// InsertByName() installs forwading rules for a VM.
//
// ingress is the VM's tap interface, egress is the hypervisors network interface, and vid is the VLAN to use to encapsulate packets coming from the VM
func InsertByName(ingress, egress string, vid int) error {
	var err error
	var i, e *net.Interface

	i, err = net.InterfaceByName(ingress)
	if err != nil {
		return fmt.Errorf("unable to get interface: %w", err)
	}
	e, err = net.InterfaceByName(egress)
	if err != nil {
		return fmt.Errorf("unable to get interface: %w", err)
	}

	return Insert(i.Index, e.Index, vid)
}

func removeProg(idx int) error {
	fname := fmt.Sprintf("%s/progs/%d", base_path, idx)
	err := os.Remove(fname)
	if err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("unable to remove pin path: %s: %w", fname, err)
	}
	return nil
}

// Delete() removes forwarding rules for the VM tap device specied by ifindex
func Delete(ingress int) error {
	var err error
	var emap *ebpf.Map
	var imap *ebpf.Map
	var vmap *ebpf.Map
	var key uint32

	emap, err = ebpf.LoadPinnedMap(egress_map_path, nil)
	if err != nil {
		return fmt.Errorf("load map: %w", err)
	}
	defer emap.Close()
	imap, err = ebpf.LoadPinnedMap(ingress_map_path, nil)
	if err != nil {
		return fmt.Errorf("load map: %w", err)
	}
	defer imap.Close()
	vmap, err = ebpf.LoadPinnedMap(vm_map_path, nil)
	if err != nil {
		return fmt.Errorf("load map: %w", err)
	}
	defer imap.Close()

	var ret error = nil

	var value xdpOutputPort
	key = uint32(ingress)
	err = emap.LookupAndDelete(&key, &value)
	if err == nil {
		key = uint32(bswap16(value.Vid)) // value is stored in network byte order

		// remove corresponding entry from ingress map
		err = imap.Delete(&key)
		if err != nil {
			ret = fmt.Errorf("delete from ingress map: %w", err)
		}	
	} else if !errors.Is(err, ebpf.ErrKeyNotExist) {
		ret = fmt.Errorf("lookup interface in egress map: %w", err)
	}
	
	// could be in the VM-to-VM map
	key = uint32(ingress)
	var peer uint32
	err = vmap.LookupAndDelete(&key, &peer)
	if err == nil {
		// remove peer entry from map as well
		err = vmap.Delete(&peer)
		if err != nil {
			ret = fmt.Errorf("delete peer key from VM map: %w", err)
		}
		// uinstall XDP program from peer's tap device
		err = removeProg(int(peer))
		if err != nil {
			ret = err
		}
	} else if !errors.Is(err, ebpf.ErrKeyNotExist) {
		ret = fmt.Errorf("delete key from VM map: %w", err)
	}

	// remove xdp program from VM tap device
	err = removeProg(ingress)
	if err != nil {
		ret = err
	}

	return ret
}

// DeleteByName() removes forwarding rules for the specified VM tap device
func DeleteByName(ingress string) error {
	i, err := net.InterfaceByName(ingress)
	if err != nil {
		return fmt.Errorf("unable to get interface: %w", err)
	}
	return Delete(i.Index)
}