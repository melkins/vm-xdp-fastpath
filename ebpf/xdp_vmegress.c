// SPDX-License-Identifier: GPL-2.0
/* Handle traffic from a VM. Expects host NICs to be into a bond
 * configured with L3+L4 hashing to spread traffic across ports.
 *
 * Copyright (c) 2019-20 David Ahern <dsahern@gmail.com>
 *
 * Modified for use in Merge by elkins@isi.edu
 */

#include "common.h"

/*
 * This program provides a fastpath for packets exiting VMs which
 * are egressing from the hypervisor to the physical network. This
 * program is installed on the tap devices for the VMs. It uses
 * a hash map from the ingress ifindex of the tap device, to a
 * struct which contains the proper VLAN tag to add, and the
 * egress ifindex of where to direct the packet.
 */

struct output_port {
	u16 vid; // VLAN ID
	u16 port; // egress port ifindex
};

// Mapping of VM ingress ifindex to output port
struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__uint(max_entries, 128);
	__type(key, u32); // ingress ifindex, data coming from vms
	__type(value, struct output_port);
} egress_ports SEC(".maps");

static __always_inline int xdp_vlan_push(struct xdp_md *ctx, __be16 vlan)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *data = (void *)(long)ctx->data;
	u8 smac[ETH_ALEN], dmac[ETH_ALEN];
	struct ethhdr *eth = data;
	struct vlan_hdr *vhdr;
	int delta = sizeof(*vhdr);
	u16 h_proto;
	int rc;

	if (eth + 1 > data_end)
		return -1;

	h_proto = eth->h_proto;
	__builtin_memcpy(smac, eth->h_source, ETH_ALEN);
	__builtin_memcpy(dmac, eth->h_dest, ETH_ALEN);

	if (bpf_xdp_adjust_head(ctx, -delta))
		return -1;

	data = (void *)(long)ctx->data;
	data_end = (void *)(long)ctx->data_end;
	eth = data;
	vhdr = data + sizeof(*eth);
	if (vhdr + 1 > data_end)
		return -1;

	vhdr->h_vlan_TCI = vlan;
	vhdr->h_vlan_encapsulated_proto = h_proto;

	__builtin_memcpy(eth->h_dest, dmac, ETH_ALEN);
	__builtin_memcpy(eth->h_source, smac, ETH_ALEN);
	eth->h_proto = bpf_htons(ETH_P_8021Q);

	return 0;
}

SEC("xdp")
int xdp_egress_prog(struct xdp_md *ctx)
{
	u32 idx = ctx->ingress_ifindex;
	struct output_port *port = bpf_map_lookup_elem(&egress_ports, &idx);
	if (!port)
		return XDP_PASS;

	// add the VLAN header
	if (xdp_vlan_push(ctx, port->vid) < 0)
		return XDP_PASS;

	// send to egress port
	return bpf_redirect(port->port, 0);
}

char _license[] SEC("license") = "GPL";
