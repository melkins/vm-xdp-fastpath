#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>

typedef __u8 u8;
typedef __u16 u16;
typedef __u32 u32;

struct vlan_hdr {
	__be16	h_vlan_TCI;
	__be16	h_vlan_encapsulated_proto;
};
#define VLAN_VID_MASK		0x0fff /* VLAN Identifier */
