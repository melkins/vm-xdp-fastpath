// SPDX-License-Identifier: GPL-2.0
/* Handle traffic from a VM. Expects host NICs to be into a bond
 * configured with L3+L4 hashing to spread traffic across ports.
 *
 * Copyright (c) 2019-20 David Ahern <dsahern@gmail.com>
 *
 * Modified for use in Merge by elkins@isi.edu
 */

#include "common.h"

/*
 * XDP program for handling VM-to-VM traffic on the same hypervisor.
 * Used as a fastpath for bypassing the Linux bridge. A hash map is
 * used to map the ingress ifindex to egress ifindex.  There is a single
 * shared map for all instances of this prog. It is to be installed on
 * the tap devices for the VMs.
 */
struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__uint(max_entries, 256); // supports 128 v2v links in total
	__type(key, u32); // ingress ifindex
	__type(value, u32); // egress ifindex
} vm_ports SEC(".maps");

SEC("xdp")
int xdp_vm2vm_prog(struct xdp_md *ctx)
{
	u32 idx = ctx->ingress_ifindex;
	u32 *port = bpf_map_lookup_elem(&vm_ports, &idx);
	if (!port)
		return XDP_PASS;

	// send to egress port
	return bpf_redirect(*port, 0);
}

char _license[] SEC("license") = "GPL";
