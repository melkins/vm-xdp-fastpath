// SPDX-License-Identifier: GPL-2.0
/* Example of L2 forwarding via XDP. FDB is a <vlan,dmac> hash table
 * returning device index to redirect packet.
 *
 * Copyright (c) 2019-2020 David Ahern <dsahern@gmail.com>
 *
 * Modified for use in Merge by elkins@isi.edu
 */

#include "common.h"

/*
 * This program provides a fastpath for packets arriving at the hypervisor
 * and destined for VMs running on it. It gets installed on all ingress
 * experiment NICs. It uses a hash map to redirect packets to the VMs
 * based on the VLAN tag in the packet.  The VLAN tag gets stripped.
 */

struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__uint(max_entries, 128);
	__type(key, u32); // VLAN ID
	__type(value, u32); // egress ifindex
} ingress_ports SEC(".maps");

SEC("xdp")
int xdp_l2fwd_prog(struct xdp_md *ctx)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *data = (void *)(long)ctx->data;
	struct vlan_hdr *vhdr = NULL;
	struct ethhdr *eth;
	u32 vid, *port;
	u8 dmac[ETH_ALEN], smac[ETH_ALEN];
	u16 h_proto = 0;
	void *nh;
	int rc;

	/* data in context points to ethernet header */
	eth = data;

	/* set pointer to header after ethernet header */
	nh = data + sizeof(*eth);
	if (nh > data_end)
		return XDP_DROP; // malformed packet

	if (eth->h_proto == bpf_htons(ETH_P_8021Q)) {
		vhdr = nh;
		if (vhdr + 1 > data_end)
			return XDP_DROP; // malformed packet

		vid = bpf_ntohs(vhdr->h_vlan_TCI) & VLAN_VID_MASK;
	} else
		return XDP_PASS; // we only handle vlan tagged packets

	port = bpf_map_lookup_elem(&ingress_ports, &vid);
	if (!port)
		return XDP_PASS;

		/* remove VLAN header before hand off to VM */
		h_proto = vhdr->h_vlan_encapsulated_proto;
		__builtin_memcpy(smac, eth->h_source, ETH_ALEN);
		__builtin_memcpy(dmac, eth->h_dest, ETH_ALEN);
		
		/* shrink packet */
		if (bpf_xdp_adjust_head(ctx, sizeof(*vhdr)))
			return XDP_PASS;

		/* reset data pointers after adjust */
		data = (void *)(long)ctx->data;
		data_end = (void *)(long)ctx->data_end;
		eth = data;
		if (eth + 1 > data_end)
			return XDP_DROP;

		/* restore the Ethernet header */
		__builtin_memcpy(eth->h_dest, dmac, ETH_ALEN);
		__builtin_memcpy(eth->h_source, smac, ETH_ALEN);
		eth->h_proto = h_proto;

		// emit the packet
	return bpf_redirect(*port, 0);
}

char _license[] SEC("license") = "GPL";
