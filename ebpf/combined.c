#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>

typedef __u8 u8;
typedef __u16 u16;
typedef __u32 u32;

struct vlan_hdr {
	__be16	h_vlan_TCI;
	__be16	h_vlan_encapsulated_proto;
};
#define VLAN_VID_MASK		0x0fff /* VLAN Identifier */
// SPDX-License-Identifier: GPL-2.0
/* Example of L2 forwarding via XDP. FDB is a <vlan,dmac> hash table
 * returning device index to redirect packet.
 *
 * Copyright (c) 2019-2020 David Ahern <dsahern@gmail.com>
 *
 * Modified for use in Merge by elkins@isi.edu
 */

/*
 * This program provides a fastpath for packets arriving at the hypervisor
 * and destined for VMs running on it. It gets installed on all ingress
 * experiment NICs. It uses a hash map to redirect packets to the VMs
 * based on the VLAN tag in the packet.  The VLAN tag gets stripped.
 */

struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__uint(max_entries, 128);
	__type(key, u32); // VLAN ID
	__type(value, u32); // egress ifindex
} ingress_ports SEC(".maps");

SEC("xdp")
int xdp_ingress_prog(struct xdp_md *ctx)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *data = (void *)(long)ctx->data;
	struct vlan_hdr *vhdr = NULL;
	struct ethhdr *eth;
	u32 vid, *port;
	u8 dmac[ETH_ALEN], smac[ETH_ALEN];
	u16 h_proto = 0;
	void *nh;
	int rc;

	/* data in context points to ethernet header */
	eth = data;

	/* set pointer to header after ethernet header */
	nh = data + sizeof(*eth);
	if (nh > data_end)
		return XDP_DROP; // malformed packet

	if (eth->h_proto == bpf_htons(ETH_P_8021Q)) {
		vhdr = nh;
		if (vhdr + 1 > data_end)
			return XDP_DROP; // malformed packet

		vid = bpf_ntohs(vhdr->h_vlan_TCI) & VLAN_VID_MASK;
	} else
		return XDP_PASS; // we only handle vlan tagged packets

	port = bpf_map_lookup_elem(&ingress_ports, &vid);
	if (!port)
		return XDP_PASS;

		/* remove VLAN header before hand off to VM */
		h_proto = vhdr->h_vlan_encapsulated_proto;
		__builtin_memcpy(smac, eth->h_source, ETH_ALEN);
		__builtin_memcpy(dmac, eth->h_dest, ETH_ALEN);
		
		/* shrink packet */
		if (bpf_xdp_adjust_head(ctx, sizeof(*vhdr)))
			return XDP_PASS;

		/* reset data pointers after adjust */
		data = (void *)(long)ctx->data;
		data_end = (void *)(long)ctx->data_end;
		eth = data;
		if (eth + 1 > data_end)
			return XDP_DROP;

		/* restore the Ethernet header */
		__builtin_memcpy(eth->h_dest, dmac, ETH_ALEN);
		__builtin_memcpy(eth->h_source, smac, ETH_ALEN);
		eth->h_proto = h_proto;

		// emit the packet
	return bpf_redirect(*port, 0);
}

/*
 * XDP program for handling VM-to-VM traffic on the same hypervisor.
 * Used as a fastpath for bypassing the Linux bridge. A hash map is
 * used to map the ingress ifindex to egress ifindex.  There is a single
 * shared map for all instances of this prog. It is to be installed on
 * the tap devices for the VMs.
 */
struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__uint(max_entries, 256); // supports 128 v2v links in total
	__type(key, u32); // ingress ifindex
	__type(value, u32); // egress ifindex
} vm_ports SEC(".maps");

SEC("xdp")
int xdp_vm_prog(struct xdp_md *ctx)
{
	u32 idx = ctx->ingress_ifindex;
	u32 *port = bpf_map_lookup_elem(&vm_ports, &idx);
	if (!port)
		return XDP_PASS;

	// send to egress port
	return bpf_redirect(*port, 0);
}

/*
 * This program provides a fastpath for packets exiting VMs which
 * are egressing from the hypervisor to the physical network. This
 * program is installed on the tap devices for the VMs. It uses
 * a hash map from the ingress ifindex of the tap device, to a
 * struct which contains the proper VLAN tag to add, and the
 * egress ifindex of where to direct the packet.
 */

struct output_port {
	u16 vid; // VLAN ID, in network byte order
	u16 port; // egress port ifindex
};

// Mapping of VM ingress ifindex to output port
struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__uint(max_entries, 128);
	__type(key, u32); // ingress ifindex, data coming from vms
	__type(value, struct output_port);
} egress_ports SEC(".maps");

static __always_inline int xdp_vlan_push(struct xdp_md *ctx, __be16 vlan)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *data = (void *)(long)ctx->data;
	u8 smac[ETH_ALEN], dmac[ETH_ALEN];
	struct ethhdr *eth = data;
	struct vlan_hdr *vhdr;
	int delta = sizeof(*vhdr);
	u16 h_proto;
	int rc;

	if (eth + 1 > data_end)
		return -1;

	h_proto = eth->h_proto;
	__builtin_memcpy(smac, eth->h_source, ETH_ALEN);
	__builtin_memcpy(dmac, eth->h_dest, ETH_ALEN);

	if (bpf_xdp_adjust_head(ctx, -delta))
		return -1;

	data = (void *)(long)ctx->data;
	data_end = (void *)(long)ctx->data_end;
	eth = data;
	vhdr = data + sizeof(*eth);
	if (vhdr + 1 > data_end)
		return -1;

	vhdr->h_vlan_TCI = vlan;
	vhdr->h_vlan_encapsulated_proto = h_proto;

	__builtin_memcpy(eth->h_dest, dmac, ETH_ALEN);
	__builtin_memcpy(eth->h_source, smac, ETH_ALEN);
	eth->h_proto = bpf_htons(ETH_P_8021Q);

	return 0;
}

SEC("xdp")
int xdp_egress_prog(struct xdp_md *ctx)
{
	u32 idx = ctx->ingress_ifindex;
	struct output_port *port = bpf_map_lookup_elem(&egress_ports, &idx);
	if (!port)
		return XDP_PASS;

	// add the VLAN header
	if (xdp_vlan_push(ctx, port->vid) < 0)
		return XDP_PASS;

	// send to egress port
	return bpf_redirect(port->port, 0);
}

char _license[] SEC("license") = "GPL";
