package main

import (
	//"errors"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/melkins/vm-xdp-fastpath/pkg/xdp"
)

func usage() {
	println("usage: xdpfp ( load | show | unload )")
	println("       xdpfp insert <vm iface> <egress iface> <vlan>")
	println("       xdpfp insert <vm iface> <vm iface> [ 0 ]")
	println("       xdpfp delete <vm iface>")
}

func main() {
	var err error = nil

	if len(os.Args) < 2 {
		usage()
		os.Exit(1)
	}
	if os.Args[1] == "load" {
		err = xdp.Load()
	} else if os.Args[1] == "unload" {
		err = xdp.Unload()
	} else if os.Args[1] == "show" {
		err = Show()
	} else if os.Args[1] == "insert" {
		if len(os.Args) < 5 {
			println("usage: xdpfp insert <vm iface> <egress iface> [ <vlan id> ]")
			os.Exit(1)
		}
		var v int64 = 0 // default to VLAN 0 == VM-to-VM connection
		if len(os.Args) == 5 {
			v, err = strconv.ParseInt(os.Args[4], 10, 32)
		}
		if err == nil {
			err = xdp.InsertByName(os.Args[2], os.Args[3], int(v))
		}
	} else if os.Args[1] == "delete" {
		if len(os.Args) != 3 {
			println("usage: xdpfp delete <vm iface>")
			os.Exit(1)
		}
		err = xdp.DeleteByName(os.Args[2])
	} else {
		usage()
	}
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	os.Exit(0)
}