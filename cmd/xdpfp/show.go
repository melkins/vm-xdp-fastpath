package main

import (
	"fmt"
	"net"
	"os"
	"text/tabwriter"

	"gitlab.com/melkins/vm-xdp-fastpath/pkg/xdp"
)

func Show() error {

	tab, err := xdp.GetForwardMap()
	if err != nil {
		return err
	}
	println("Egress Table (VM->Internet)")
	w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
	fmt.Fprintln(w, "Source\t->\tDest\tVlan")
	fmt.Fprintln(w, "-----\t\t----\t----")
		
	for key, value := range tab.Egress {
		in, err := net.InterfaceByIndex(int(key))
		if err != nil {
			return fmt.Errorf("unable to get interface name: ifindex=%d: %w", key, err)
		}
		out, err := net.InterfaceByIndex(int(value.Port))
		if err != nil {
			return fmt.Errorf("unable to get interface name: ifindex=%d: %w", value.Port, err)
		}

		
		fmt.Fprintf(w, "%d (%s)\t\t%d (%s)\t%d\n", key, in.Name, value.Port, out.Name, value.Vid)
	}
	w.Flush()

	println("\nIngress Table (Internet->VM)")
	w = tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
	fmt.Fprintln(w, "Vlan\t->\tDest")
	fmt.Fprintln(w, "----\t\t----")
	for key, value := range tab.Ingress {
		out, err := net.InterfaceByIndex(int(value))
		if err != nil {
			return fmt.Errorf("unable to get interface name: ifindex=%d: %w", value, err)
		}
		fmt.Fprintf(w, "%d\t\t%d (%s)\n", key, value, out.Name)
	}
	w.Flush()

	return nil
}
