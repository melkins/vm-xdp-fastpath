# VM XDP fastpath

This repository contains proof of concept code for accelatering network traffic to virtual machines by using XDP to bypass the Linux bridge and deliver packets
directly to the VM tap devices from the hypervisor's network interface.

The code is derived from David Ahern's examples (https://github.com/dsahern/bpf-progs), and he describes it in detail in this presentation from the Netdev conference:
https://www.youtube.com/watch?v=l9C-ANkN1-Q

The code was taken from commit f7572947e3e6d67936758633b65146483e17d4d6

RX vlan offloading must be deactivated on the hypervisor network interfaces, or else the XDP program can't see the VLAN tags.
ethtool -K <dev> rxvlan off

The VM guests need to have TX offloading disable for TCP to work
ethtool -K <dev> tx off tso off

VMs can be directly connected by specifying two tap devices and leaving off the vlan (or using 0):
xdpfp insert tap1 tap2

```
# ./xdpfp show
Egress Table (VM->Internet)
Source       -> Dest       Vlan
-----           ----       ----
142 (mtap5)     8 (ens1f3) 646
155 (mtap18)    7 (ens1f2) 2016
147 (mtap10)    7 (ens1f2) 3012
138 (mtap2)     4 (ens1f0) 223
135 (mtap1)     5 (ens1f1) 1695
154 (mtap17)    8 (ens1f3) 2224
145 (mtap8)     7 (ens1f2) 2626
144 (mtap7)     8 (ens1f3) 1557
151 (mtap14)    8 (ens1f3) 1689
141 (mtap4)     4 (ens1f0) 2157
158 (mtap21)    5 (ens1f1) 2901
157 (mtap20)    7 (ens1f2) 3056

Ingress Table (Internet->VM)
Vlan -> Dest
----    ----
1557    144 (mtap7)
3056    157 (mtap20)
2901    158 (mtap21)
223     138 (mtap2)
2016    155 (mtap18)
2626    145 (mtap8)
1695    135 (mtap1)
646     142 (mtap5)
1689    151 (mtap14)
2224    154 (mtap17)
3012    147 (mtap10)
2157    141 (mtap4)
```
